import React, { useEffect } from 'react';
import '../../src/scss/StarBackground.scss';

const StarBackground = ({ maxStars }) => {
  useEffect(() => {
    // Create stars
    for (let i = 0; i < maxStars; i++) {
      const star = document.createElement('div');
      star.classList.add('star');
      star.style.width = `${Math.random() * 3}px`;
      star.style.height = star.style.width;
      star.style.top = `${Math.random() * 100}%`;
      star.style.left = `${Math.random() * 100}%`;
      star.style.animationDelay = `${Math.random() * 2}s`;
      document.getElementById('stars').appendChild(star);
    }
  }, [maxStars]);

  return (
    <div id="stars"></div>
  );
};

export default StarBackground;
